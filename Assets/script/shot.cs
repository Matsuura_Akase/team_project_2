﻿using UnityEngine;
using System.Collections;

public class shot : MonoBehaviour
{
    private Vector3 Downposition;
	public GameObject prefab;
	public static bool canShot;
	public static float count;
    // Use this for initialization
    void Start()
    {
		canShot = true;
    }

    // Update is called once per frame
    void Update()
    {
		if (canShot == false) {
			count +=Time.deltaTime;
			if (count >= 3.0f)
			{
				canShot = true;
				count = 0;
			}
		}
    }

    void OnMouseDown()
    {
		if (canShot == true)
		{
			Downposition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
			//Debug.Log(Downposition);
		}
    }
    void OnMouseUp()
    {
		if (canShot == true)
		{
			var Upposition = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
			//Debug.Log(Upposition);
			var dir = Upposition - Downposition;
			var throwingStar = (GameObject)Instantiate(prefab, new Vector3(5, -13.5f, -1), transform.rotation);
			syuriken syuriken = throwingStar.GetComponent<syuriken>();
			syuriken.vec = dir;
			canShot = false;

		}

		//new Vector2(directtion.x, directtion.y);

		//進行方向に向きを変える場合
		//var dir = Upposition - Downposition;
		//var Angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg-90.0f;
		//Instantiate(prefab, new Vector3(5, -13.5f, -1), Quaternion.Euler(0.0f,0.0f,Angle));
		
    }
}