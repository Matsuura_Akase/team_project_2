﻿using UnityEngine;
using System.Collections;

public class syuriken : MonoBehaviour
{
	//移動の早さ
	public float speed = 7;

	public Vector3 vec;

	// Use this for initialization
	void Start()
	{
		//4秒たったら自動で消える
		Destroy(this.gameObject, 4f);
	}

	// Update is called once per frame
	void Update()
	{
		Vector3 scale = new Vector3(1, 1,0);
		Vector3 dir = vec.normalized;

		//座標移動
		transform.position += dir * speed * Time.deltaTime;

		transform.localScale -= dir * speed;
		//if (transform.position.y > -13.5)
		//{
			//徐々に手裏剣を小さく
			//float i;
			//for (i = 0.005f; i < 1; i++)
			//{
				//transform.localScale -= i * scale.normalized;
			//}
		//}
		transform.Rotate(0,0,10);
	}
	void OnTriggerEnter2D (Collider2D c){
		//敵の削除
		Destroy(c.gameObject);
		//手裏剣の削除
		Destroy(this.gameObject);

		shot.canShot = true;
		shot.count = 0;

	}
}