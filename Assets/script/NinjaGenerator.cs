﻿using UnityEngine;
using System.Collections;

public class NinjaGenerator : MonoBehaviour {

	//Ninjaのプレハブ
	public GameObject Ninja1;

	// Startメソッドをコルーチンとして呼び出す
	IEnumerator Start()
	{
		while (true)
		{
			// 弾をNinja1の位置/角度で作成
			Instantiate(Ninja1, transform.position, transform.rotation);
			// 0.05秒待つ
			yield return new WaitForSeconds(5f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
