﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	// スコアを表示するGUIText
	public GUIText scoreGUIText;
	// スコア
	private int score;

	void Start()
	{
	}

	void Update()
	{
		// スコアを表示する
		scoreGUIText.text = score.ToString();
	}

	// ゲーム開始前の状態に戻す
	private void Initialize()
	{
		// スコアを0に戻す
		score = 0;

	}
	// ポイントの追加
	public void AddPoint(int point)
	{
		score = score + point;
	}
}