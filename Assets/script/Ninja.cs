﻿using UnityEngine;
using System.Collections;

public class Ninja : MonoBehaviour {
	// スコアのポイント
	public int point = 100;

	//速度
	public int speed = 1;
        void Start ()
        {
                rigidbody2D.velocity = transform.right.normalized* speed;
        }


		void OnTriggerEnter2D(Collider2D c)
		{
			// レイヤー名を取得
			string layerName = LayerMask.LayerToName(c.gameObject.layer);
			// レイヤー名がsyuriken以外の時は何も行わない
			if (layerName != "syuriken") return;
			// スコアコンポーネントを取得してポイントを追加
			FindObjectOfType<Score>().AddPoint(point);
		}



	// Update is called once per frame
	void Update () {
	
	}
}
